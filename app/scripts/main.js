// slide
$(document).ready(function () {
  $(".header-slide").slick({
    responsive: [{ breakpoint: 767, settings: { arrows: !1 } }],
    prevArrow: false,
    nextArrow: false,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 200000,
    dots: false,
  });
});

// toggle menu
let iconToggle = document.querySelector(".header-icon-toggle");
let headerMenu = document.querySelector(".header-menu");
let headerTopContent = document.querySelector(".header-top-content");
let headerIconClose = document.querySelector(".header-icon-close");
let expandClass = "is-expand";
if (iconToggle) {
  iconToggle.addEventListener("click", () => {
    headerTopContent.style.alignItems = "stretch";
    headerMenu.classList.add(expandClass);
  });
}
if (headerIconClose) {
  headerIconClose.addEventListener("click", () => {
    headerTopContent.style.alignItems = "center";
    headerMenu.classList.remove(expandClass);
  });
}

window.addEventListener("click", (e) => {
  if (
    !headerMenu.contains(e.target) &&
    !e.target.matches(".header-icon-toggle")
  ) {
    headerTopContent.style.alignItems = "center";
    headerMenu.classList.remove(expandClass);
  }
});

// back to top
window.addEventListener("load", function () {
  let btnBackTop = document.querySelector(".footer-back-top");
  window.onscroll = function () {
    scrollFunction();
  };
  function scrollFunction() {
    if (
      document.body.scrollTop > 300 ||
      document.documentElement.scrollTop > 300
    ) {
      btnBackTop.style.display = "block";
    } else {
      btnBackTop.style.display = "none";
    }
  }
  function topFunction() {
    window.scrollTo({ top: 0, behavior: "smooth" });
  }
  btnBackTop.addEventListener("click", topFunction);
});

window.addEventListener("load", function () {
  // dropdown
  const dropdownItems = document.querySelectorAll(
    "#lightdropdown .contact-dropdown-item"
  );
  const dropdownSelect = document.querySelector(
    "#lightdropdown .contact-dropdown-select"
  );
  const dropdownSelectText = document.querySelector(
    "#lightdropdown .contact-dropdown-selected"
  );
  const dropdownList = document.querySelector(
    "#lightdropdown .contact-dropdown-list"
  );
  const dropdownCaret = document.querySelector(
    "#lightdropdown .contact-dropdown-caret"
  );

  dropdownSelect.addEventListener("click", function () {
    dropdownList.classList.toggle("show");
    dropdownCaret.classList.toggle("fa-angle-up");
  });

  function handleSelectDropdown(e) {
    const { value } = e.target.dataset;
    dropdownSelectText.textContent = value;
    dropdownList.classList.remove("show");
    dropdownCaret.classList.remove("fa-angle-up");
  }

  dropdownItems.forEach((el) =>
    el.addEventListener("click", handleSelectDropdown)
  );

  // tab
  const tabs = document.querySelectorAll(".menu-tab-item");
  const tabsContent = document.querySelectorAll(".menu-content-list");

  function handleChangeTab(e) {
    const tabId = e.target.dataset.tab;
    tabs.forEach((el) => el.classList.remove("active"));
    e.target.classList.add("active");
    tabsContent.forEach((el) => {
      el.classList.remove("active");
      if (el.getAttribute("data-tab") === tabId) {
        el.classList.add("active");
      }
    });
  }

  tabs.forEach((el) => el.addEventListener("click", handleChangeTab));
});
